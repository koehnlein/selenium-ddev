# Selenium for DDEV

This project provides a docker setup with selenium server and Chrome browser, optimized for your DDEV setup.

## Usage

Checkout and start project `ddev start`

Configure WebDriver in the project you want to test in `acceptance.suite.yml`

```yaml
modules:
  config:
    WebDriver:
      host: selenium-hub
      browser: chrome
```

### Watch

To watch the tests running in browser, you can use any VNC client and connect to `localhost` on default VNC port 5900. The password is `secret`.
